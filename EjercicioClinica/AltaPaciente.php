<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .login-page {
            width: 500px;
            padding: 8% 0 0;
            margin: auto;
        }

        .form {
            position: relative;
            z-index: 1;
            background: #FFFFFF;
            max-width: 360px;
            margin: 0 auto 100px;
            padding: 45px;
            text-align: center;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        }

        .form input {
            font-family: "Roboto", sans-serif;
            outline: 0;
            background: #f2f2f2;
            width: 100%;
            border: 0;
            margin: 0 0 15px;
            padding: 15px;
            box-sizing: border-box;
            font-size: 14px;
        }

        .form select {
            font-family: "Roboto", sans-serif;
            outline: 0;
            background: #f2f2f2;
            width: 100%;
            border: 0;
            margin: 0 0 15px;
            padding: 15px;
            box-sizing: border-box;
            font-size: 14px;

        }

        .form button {
            font-family: "Roboto", sans-serif;
            text-transform: uppercase;
            outline: 0;
            background: #4CAF50;
            width: 100%;
            border: 0;
            padding: 15px;
            color: #FFFFFF;
            font-size: 14px;
            -webkit-transition: all 0.3 ease;
            transition: all 0.3 ease;
            cursor: pointer;
        }

        .form button:hover,
        .form button:active,
        .form button:focus {
            background: #43A047;
        }

        .form .message {
            margin: 15px 0 0;
            color: #b3b3b3;
            font-size: 12px;
        }

        .form .message a {
            color: #4CAF50;
            text-decoration: none;
        }

        .form .register-form {
            display: none;
        }

        .container {
            position: relative;
            z-index: 1;
            max-width: 300px;
            margin: 0 auto;
        }

        .container:before,
        .container:after {
            content: "";
            display: block;
            clear: both;
        }

        .container .info {
            margin: 50px auto;
            text-align: center;
        }

        .container .info h1 {
            margin: 0 0 15px;
            padding: 0;
            font-size: 36px;
            font-weight: 300;
            color: #1a1a1a;
        }

        .container .info span {
            color: #4d4d4d;
            font-size: 12px;
        }

        .container .info span a {
            color: #000000;
            text-decoration: none;
        }

        .container .info span .fa {
            color: #EF3B3A;
        }

        span {
            color: red;
        }

        body {
            background: #76b852;
            /* fallback for old browsers */
            background: -webkit-linear-gradient(right, #76b852, #8DC26F);
            background: -moz-linear-gradient(right, #76b852, #8DC26F);
            background: -o-linear-gradient(right, #76b852, #8DC26F);
            background: linear-gradient(to left, #76b852, #8DC26F);
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            text-align: center;

        }
    </style>
</head>

<body>
    <?php $hoy = date("Y-m-d");
    session_start();
    if ($_SESSION["rol"] == "asistente" || $_SESSION["rol"] == "administrador") {
        if (isset($_POST["enviar"])) {
            $ErrorNombre = "";
            $ErrorApe = "";
            $ErrorContra = "";
            $ErrorFecha = "";
            $ErrorDni= "";

            if (!preg_match("/[0-9]{8}[A-Z]/", $_POST["dni"])){
                $valido=false;
                $ErrorDni= "El DNI debe tener 8 numeros y 1 letra mayúscula<br/>";
            }
            else{
                $letra = substr($_POST["dni"], -1);
                $numeros = substr($_POST["dni"], 0, -1); 
                if (substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros % 23, 1) == $letra && strlen($letra) == 1 && strlen($numeros) == 8) {
                    $valido = true;
                }
                else{
                    $valido=false;
                    $ErrorDni = " Dni inválido<br/>";
                }
            }
            if($valido){
            if (!preg_match("/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ\s]+$/", $_POST["name"])) {
                $ErrorNombre = " El nombre tiene caracteres inválidos<br/>";
            } else if (!preg_match("/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ\s]+$/", $_POST["ape"])) {
                $ErrorApe = " El apellido tiene caracteres inválidos<br/>";
            } else if ($_POST["date"] > $hoy) {
                $ErrorFecha = " Fecha introducida inválida<br/>";
            } else if ($_POST["password"] != $_POST["password2"]) {
                $ErrorContra = "Las cotraseñas son distintas<br/>";
            } else {
                $conectar = mysqli_connect('localhost', "$_SESSION[rol]", "$_SESSION[rol]", 'consulta');
                $usuario = "SELECT dniUsu from usuarios where dniUsu='$_POST[dni]'";
                $select = mysqli_query($conectar, $usuario);
                $fila = mysqli_fetch_assoc($select);
                if (empty($fila)) {
                    $password = hash_hmac('sha512', $_POST["password"], 'secret');
                    $insert1 = "INSERT INTO usuarios (dniUsu,usuLogin,usuPassword,usuEstado,usutipo) VALUES ('$_POST[dni]', '$_POST[login]', '$password', 'Activo', 'Paciente')";
                    $select = mysqli_query($conectar, $insert1);
                    $insert2 = "INSERT INTO pacientes (dniPac,pacNombres,pacApellidos,pacFechaNacimiento,pacSexo) VALUES ('$_POST[dni]', '$_POST[name]', '$_POST[ape]', '$_POST[date]', '$_POST[sexo]')";
                    $select = mysqli_query($conectar, $insert2);
                    echo "<h3>Cuenta creada correctamente</h3>";
                } else {
                    echo "<h3>Ese usuario ya existe </h3>";
                }
            }
        }
        }
    } else {
        echo "El rol no es ni Asistente ni Administrador no tiene permiso";
    }
    ?>
    <div class="login-page">
        <div class="form">
            <h1>ADSI VIRTUAL</h1>
            <h3>INSERTAR PACIENTE</h3>
            <form method="POST" action="#" class="login-form">
                <label>Identifiacion</label>
                <input type="text" name="dni" value="<?php if (isset($_POST["enviar"])) { if ($ErrorDni == "") {
                                                            echo $_POST["dni"];}
                                                        } ?>" required><span><?php if (isset($_POST["enviar"])) {
                                                                        echo $ErrorDni;
                                                                    } ?> </span><br />
                    <label>Nombre</label>
                    <input type="text" name="name" value="<?php if (isset($_POST["enviar"])) {
                                                                 if ($ErrorNombre == "") {
                                                                    echo $_POST["name"];
                                                                }
                                                            } ?>" required><span><?php if (isset($_POST["enviar"])) {
                                                                        echo $ErrorNombre;
                                                                    } ?></span><br />
                    <label>Apellidos</label>
                    <input type="text" name="ape" value="<?php if (isset($_POST["enviar"])) {
                                                                if ($ErrorApe == "") {
                                                                    echo $_POST["ape"];
                                                                }
                                                            } ?>"><span><?php if (isset($_POST["enviar"])) {
                                                                        echo $ErrorApe;
                                                                    } ?></span><br />
                    <label>Fecha Nacimiento</label>

                    <input type="date" name="date" max="<?php echo $hoy; ?>" value="<?php if (isset($_POST["enviar"])) { if ($ErrorFecha == "") {
                                                            echo $_POST["date"];}
                                                        } ?>" required><span><?php if (isset($_POST["enviar"])) {
                                                                                            echo $ErrorFecha;
                                                                                        } ?></span><br />
                    <label>Sexo</label>
                    <select name="sexo">
                        <option value="Masculino">Masculino</option>
                        <option value="Femenino">Femenino</option>
                    </select><br /><br /> <br />
                    <label>Nombre Usuario</label>
                    <input type="text" name="login" value="<?php if (isset($_POST["enviar"])) {
                                                                echo $_POST["login"];
                                                            } ?>" required><br />
                    <label>Contraseña</label>
                    <input type="password" name="password" required><span><?php if (isset($_POST["enviar"])) {
                                                                        echo $ErrorContra;
                                                                    } ?> </span><br />
                    <label>Repita Contraseña</label>
                    <input type="password" name="password2" required><br /><br />
                    <input type="submit" name="enviar" value="Enviar">
            </form>
        </div>
    </div>
</body>

</html>