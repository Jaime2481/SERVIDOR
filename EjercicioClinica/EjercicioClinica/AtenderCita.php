<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .login-page {
            width: 500px;
            padding: 8% 0 0;
            margin: auto;
        }

        .form {
            position: relative;
            z-index: 1;
            background: #FFFFFF;
            max-width: 560px;
            margin: 0 auto 100px;
            padding: 45px;
            text-align: center;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        }

        .form input {
            font-family: "Roboto", sans-serif;
            outline: 0;
            background: #f2f2f2;
            width: 100%;
            border: 0;
            margin: 0 0 15px;
            padding: 15px;
            box-sizing: border-box;
            font-size: 14px;
        }

        .form button {
            font-family: "Roboto", sans-serif;
            text-transform: uppercase;
            outline: 0;
            background: #4CAF50;
            width: 100%;
            border: 0;
            padding: 15px;
            color: #FFFFFF;
            font-size: 14px;
            -webkit-transition: all 0.3 ease;
            transition: all 0.3 ease;
            cursor: pointer;
        }

        .form button:hover,
        .form button:active,
        .form button:focus {
            background: #43A047;
        }

        .form .message {
            margin: 15px 0 0;
            color: #b3b3b3;
            font-size: 12px;
        }

        .form .message a {
            color: #4CAF50;
            text-decoration: none;
        }

        .form .register-form {
            display: none;
        }

        .container {
            position: relative;
            z-index: 1;
            max-width: 500px;
            margin: 0 auto;
        }

        .container:before,
        .container:after {
            content: "";
            display: block;
            clear: both;
        }

        .container .info {
            margin: 50px auto;
            text-align: center;
        }

        span {
            color: blue;
        }

        .container .info h1 {
            margin: 0 0 15px;
            padding: 0;
            font-size: 36px;
            font-weight: 300;
            color: #1a1a1a;
        }

        .container .info span {
            color: #4d4d4d;
            font-size: 12px;
        }

        .container .info span a {
            color: #000000;
            text-decoration: none;
        }

        .container .info span .fa {
            color: #EF3B3A;
        }

        body {
            background: #76b852;
            /* fallback for old browsers */
            background: -webkit-linear-gradient(right, #76b852, #8DC26F);
            background: -moz-linear-gradient(right, #76b852, #8DC26F);
            background: -o-linear-gradient(right, #76b852, #8DC26F);
            background: linear-gradient(to left, #76b852, #8DC26F);
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
    </style>
</head>

<body>
    <?php
    session_start();

    if ($_SESSION["rol"] == "medico") {
    ?>
        <div class="login-page">
            <div class="form">
                <h1>Atender Cita</h1>

                <form action="#" method="POST" class="login-form">
                    <label>Paciente:</label>
                    <input type="text" value="<?php if (!isset($_POST["observaciones"])) {
                                                    echo $_POST["atender"];
                                                } else {
                                                    echo $_POST["paciente"];
                                                } ?> " name="paciente">
                    <br /><br />
                    <label>Observaciones</label><br />
                    <textarea name="coment" rows="10" cols="50" required></textarea><br /><br />
                    <input type="submit" value="Enviar" name="observaciones">
                    <?php
                    if (isset($_POST["observaciones"])) {
                        if (empty($_POST["coment"])) {
                            echo "Las observaciones no pueden quedar vacías";
                        } else {
                            $conectar = mysqli_connect('localhost', "$_SESSION[rol]", "$_SESSION[rol]", 'consulta');
                            $sql = "UPDATE citas SET citEstado='Atendido' , CitObservaciones='$_POST[coment]' WHERE citPaciente='$_POST[paciente]'";
                            $select = mysqli_query($conectar, $sql);
                            echo "<span>Cita actualizada correctamente</span><br/></br/>";
                        }
                    ?>
                </form>
                <form method="POST" action="VerCitasPendientes.php" class="login-form">
                    <input type="submit" value="Volver a ver las citas pendientes">
                </form>
        <?php
                    }
                }
        ?>
            </div>
        </div>

</body>

</html>