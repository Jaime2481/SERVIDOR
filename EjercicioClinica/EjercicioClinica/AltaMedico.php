<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .login-page {
            width: 500px;
            padding: 8% 0 0;
            margin: auto;
        }

        .form {
            position: relative;
            z-index: 1;
            background: #FFFFFF;
            max-width: 360px;
            margin: 0 auto 100px;
            padding: 45px;
            text-align: center;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        }

        .form input {
            font-family: "Roboto", sans-serif;
            outline: 0;
            background: #f2f2f2;
            width: 100%;
            border: 0;
            margin: 0 0 15px;
            padding: 15px;
            box-sizing: border-box;
            font-size: 14px;
        }

        .form select {
            font-family: "Roboto", sans-serif;
            outline: 0;
            background: #f2f2f2;
            width: 100%;
            border: 0;
            margin: 0 0 15px;
            padding: 15px;
            box-sizing: border-box;
            font-size: 14px;

        }

        .form button {
            font-family: "Roboto", sans-serif;
            text-transform: uppercase;
            outline: 0;
            background: #4CAF50;
            width: 100%;
            border: 0;
            padding: 15px;
            color: #FFFFFF;
            font-size: 14px;
            -webkit-transition: all 0.3 ease;
            transition: all 0.3 ease;
            cursor: pointer;
        }

        .form button:hover,
        .form button:active,
        .form button:focus {
            background: #43A047;
        }

        .form .message {
            margin: 15px 0 0;
            color: #b3b3b3;
            font-size: 12px;
        }

        .form .message a {
            color: #4CAF50;
            text-decoration: none;
        }

        .form .register-form {
            display: none;
        }

        .container {
            position: relative;
            z-index: 1;
            max-width: 300px;
            margin: 0 auto;
        }

        .container:before,
        .container:after {
            content: "";
            display: block;
            clear: both;
        }

        .container .info {
            margin: 50px auto;
            text-align: center;
        }

        .container .info h1 {
            margin: 0 0 15px;
            padding: 0;
            font-size: 36px;
            font-weight: 300;
            color: #1a1a1a;
        }

        .container .info span {
            color: #4d4d4d;
            font-size: 12px;
        }

        .container .info span a {
            color: #000000;
            text-decoration: none;
        }

        .container .info span .fa {
            color: #EF3B3A;
        }

        body {
            background: #76b852;
            /* fallback for old browsers */
            background: -webkit-linear-gradient(right, #76b852, #8DC26F);
            background: -moz-linear-gradient(right, #76b852, #8DC26F);
            background: -o-linear-gradient(right, #76b852, #8DC26F);
            background: linear-gradient(to left, #76b852, #8DC26F);
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            text-align: center;
        }
    </style>
</head>

<body>
    <?php $hoy = date("Y-m-d"); 
    session_start();
    if ($_SESSION["rol"] == "asistente" || $_SESSION["rol"] == "administrador") {
        if (isset($_POST["enviar"])) {
            $ErrorNombre = "";
            $ErrorApe = "";
            $ErrorEspe = "";
            $ErrorPhone = "";
            $ErrorEmail = "";
            $ErrorDni = "";
            $ErrorContra = "";
            if (!preg_match("/[0-9]{8}[A-Z]/", $_POST["dni"])) {
                $valido = false;
                $ErrorDni = "El DNI debe tener 8 numeros y 1 letra mayúscula<br/>";
            } else {
                $letra = substr($_POST["dni"], -1);
                $numeros = substr($_POST["dni"], 0, -1);
                if (substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros % 23, 1) == $letra && strlen($letra) == 1 && strlen($numeros) == 8) {
                    $valido = true;
                } else {
                    $valido = false;
                    $ErrorDni = " Dni inválido<br/>";
                }
            }
            if ($valido) {
                if ($_POST["password"] != $_POST["password2"]) {
                    $ErrorContra = "Las cotraseñas son distintas";
                } else if (!preg_match("/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ\s]+$/", $_POST["name"])) {
                    $ErrorNombre = " El nombre tiene caracteres inválidos";
                } else  if (!substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros % 23, 1) == $letra && strlen($letra) == 1 && strlen($numeros) == 8) {
                    $ErrorDni = " Dni inválido";
                } else if (!preg_match("/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ\s]+$/", $_POST["ape"])) {
                    $ErrorApe = " El apellido tiene caracteres inválidos";
                } else if (!preg_match("/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ\s]+$/", $_POST["espe"])) {
                    $ErrorEspe = " La especialidad tiene caracteres inválidos";
                } else if (!preg_match("/^[0-9]{8}/", $_POST["phone"])) {
                    $ErrorPhone = " El telefono es inválido";
                } else if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                    $ErrorEmail = " Formato de Email invalido";
                } else {

                    $conectar = mysqli_connect('localhost', "$_SESSION[rol]", "$_SESSION[rol]", 'consulta');
                    $usuario = "SELECT dniUsu from usuarios where dniUsu='$_POST[dni]'";
                    $select = mysqli_query($conectar, $usuario);
                    $fila = mysqli_fetch_assoc($select);
                    if (empty($fila)) {
                        $password = hash_hmac('sha512', $_POST["password"], 'secret');
                        $insert = "INSERT INTO medicos (dniMed,medNombres,medApellidos,medEspecialidad,medTelefono,medCorreo) VALUES ('$_POST[dni]', '$_POST[name]', '$_POST[ape]', '$_POST[espe]', '$_POST[phone]','$_POST[email]')";
                        $select = mysqli_query($conectar, $insert);
                        $insert = "INSERT INTO usuarios (dniUsu,usuLogin,usuPassword,usuEstado,usutipo) VALUES ('$_POST[dni]', '$_POST[login]', '$password', '$_POST[estado]', 'Medico')";
                        $select = mysqli_query($conectar, $insert);
                        echo "<h3>Cuenta creada correctamente</h3>";
                    } else {
                        echo "<h3>Ese usuario ya existe </h3>";
                    }
                }
            }
        }
    } else {
        echo "El rol no es ni Asistente ni Administrador no tiene permiso";
    }
    ?>
    <div class="login-page">
        <div class="form">
            <h1>ADSI VIRTUAL</h1>
            <h3>INSERTAR MEDICO</h3>
            <form method="POST" action="#" class="login-form">
                <label>Nombre</label>
                <input type="text" name="name" value="<?php if (isset($_POST["enviar"])) {
                                                            if ($ErrorNombre == "") {
                                                                echo $_POST["name"];
                                                            }
                                                        } ?>"><span><?php if (isset($_POST["enviar"])) {
                                                                        echo $ErrorNombre;
                                                                    } ?></span><br />
                <label>Apellidos</label>
                <input type="text" name="ape" value="<?php if (isset($_POST["enviar"])) {
                                                            if ($ErrorApe == "") {
                                                                echo $_POST["ape"];
                                                            }
                                                        } ?>"><span><?php if (isset($_POST["enviar"])) {
                                                                        echo $ErrorApe;
                                                                    } ?></span><br />
                <label>Especialidad</label>
                <input type="text" name="espe" value="<?php if (isset($_POST["enviar"])) {
                                                            if ($ErrorEspe == "") {
                                                                echo $_POST["espe"];
                                                            }
                                                        } ?>"><span><?php if (isset($_POST["enviar"])) {
                                                                        echo $ErrorEspe;
                                                                    } ?></span><br />
                <label>Telefono</label>
                <input type="tel" name="phone" value="<?php if (isset($_POST["enviar"])) {
                                                            if ($ErrorPhone == "") {
                                                                echo $_POST["phone"];
                                                            }
                                                        } ?>"><span><?php if (isset($_POST["enviar"])) {
                                                                        echo $ErrorPhone;
                                                                    } ?></span><br />
                <label>Email</label>
                <input type="text" name="email" value="<?php if (isset($_POST["enviar"])) {
                                                            if ($ErrorEmail == "") {
                                                                echo $_POST["email"];
                                                            }
                                                        } ?>"><span><?php if (isset($_POST["enviar"])) {
                                                                        echo $ErrorEmail;
                                                                    } ?></span><br />
                <label>DNI</label>
                <input type="text" name="dni" value="<?php if (isset($_POST["enviar"])) {
                                                            if ($ErrorDni == "") {
                                                                echo $_POST["dni"];
                                                            }
                                                        } ?>"><span><?php if (isset($_POST["enviar"])) {
                                                                        echo $ErrorDni;
                                                                    } ?></span<br /><br />
                    <label>Nombre Usuario</label>
                    <input type="text" name="login" value="<?php if (isset($_POST["enviar"])) {
                                                                echo $_POST["login"];
                                                            } ?>"><br />
                    <label>Contraseña</label>
                    <input type="password" name="password"><span><?php if (isset($_POST["enviar"])) {
                                                                        echo $ErrorContra;
                                                                    } ?></span><br />
                    <label>Repita Contraseña</label>
                    <input type="password" name="password2"><br />
                    <label>Estado</label>
                    <select name="estado">
                        <option value="Activo">Activo</option>
                        <option value="Inactivo">Inactivo</option>
                    </select><br />
                    <input type="submit" name="enviar" value="Enviar">
            </form>
        </div>
    </div>
</body>

</html>