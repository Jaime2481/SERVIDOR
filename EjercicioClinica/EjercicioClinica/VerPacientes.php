<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
    h1 {
      font-size: 30px;
      color: #fff;
      text-transform: uppercase;
      font-weight: 300;
      text-align: center;
      margin-bottom: 15px;
    }

    table {
      width: 100%;
      table-layout: fixed;
    }

    .tbl-header {
      background-color: rgba(255, 255, 255, 0.3);
    }

    .tbl-content {
      overflow-x: auto;
      margin-top: 0px;
      border: 1px solid rgba(255, 255, 255, 0.3);
    }

    th {
      padding: 20px 15px;
      text-align: left;
      font-weight: 500;
      font-size: 12px;
      color: #fff;
      text-transform: uppercase;
    }

    td {
      padding: 15px;
      text-align: left;
      vertical-align: middle;
      font-weight: 300;
      font-size: 12px;
      color: #fff;
      border-bottom: solid 1px rgba(255, 255, 255, 0.1);
    }



    @import url(https://fonts.googleapis.com/css?family=Roboto:400,500,300,700);

    body {
      background: -webkit-linear-gradient(left, #25c481, #25b7c4);
      background: linear-gradient(to right, #25c481, #25b7c4);
      font-family: 'Roboto', sans-serif;
    }

    section {
      margin: 50px;
    }


  </style>
</head>

<body>
    <?php
    session_start();
    $conectar = mysqli_connect('localhost', "$_SESSION[rol]", "$_SESSION[rol]", 'consulta');
    if ($_SESSION["rol"] == "asistente") {
        $select = "SELECT * FROM pacientes";
        $consulta = mysqli_query($conectar, $select);
        echo "<div class='tbl-header'>";
        echo "<table cellpadding='0' cellspacing='0' border='0'>";
        echo "<thead>";
        echo "<th>Identificacion</th><th>Nombre</th><th>Apellidos</th><th>Fecha Nacimiento</th><th>Sexo</th>";
        echo "</thead>";
        echo "</table>";
        echo "</div>";
        echo "<div class='tbl-content'>";
        echo "<table cellpadding='0' cellspacing='0' border='0'>";
        echo "<tbody>";
        while ($valores = mysqli_fetch_array($consulta)) {
            echo "<tr>";
            echo "<td>$valores[dniPac]</td><td>$valores[pacNombres]</td><td>$valores[pacApellidos]</td><td>$valores[pacFechaNacimiento]</td><td>$valores[pacSexo]</td> ";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } elseif ($_SESSION["rol"] == "medico") {
        $select = "SELECT DISTINCT p.dniPac,p.pacNombres,p.pacApellidos,p.pacFechaNacimiento,p.pacSexo FROM citas c,pacientes p where c.citMedico='$_SESSION[idusuario]'";
        $consulta = mysqli_query($conectar, $select);
        echo "<div class='tbl-header'>";
        echo "<table cellpadding='0' cellspacing='0' border='0'>";
        echo "<thead>";
        echo "<th>Identificacion</th><th>Nombre</th><th>Apellidos</th><th>Fecha Nacimiento</th><th>Sexo</th>";
        echo "</thead>";
        echo "</table>";
        echo "</div>";
        echo "<div class='tbl-content'>";
        echo "<table cellpadding='0' cellspacing='0' border='0'>";
        echo "<tbody>";
        while ($valores = mysqli_fetch_array($consulta)) {
            echo "<tr>";
            echo "<td>$valores[dniPac]</td><td>$valores[pacNombres]</td><td>$valores[pacApellidos]</td><td>$valores[pacFechaNacimiento]</td><td>$valores[pacSexo]</td> ";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }
    ?>
</body>

</html>