<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
    h1 {
      font-size: 30px;
      color: #fff;
      text-transform: uppercase;
      font-weight: 300;
      text-align: center;
      margin-bottom: 15px;
    }

    table {
      width: 100%;
      table-layout: fixed;
    }

    .tbl-header {
      background-color: rgba(255, 255, 255, 0.3);
    }

    .tbl-content {
      overflow-x: auto;
      margin-top: 0px;
      border: 1px solid rgba(255, 255, 255, 0.3);
    }

    th {
      padding: 20px 15px;
      text-align: left;
      font-weight: 500;
      font-size: 12px;
      color: #fff;
      text-transform: uppercase;
    }

    td {
      padding: 15px;
      text-align: left;
      vertical-align: middle;
      font-weight: 300;
      font-size: 12px;
      color: #fff;
      border-bottom: solid 1px rgba(255, 255, 255, 0.1);
    }


    @import url(https://fonts.googleapis.com/css?family=Roboto:400,500,300,700);

    body {
      background: -webkit-linear-gradient(left, #25c481, #25b7c4);
      background: linear-gradient(to right, #25c481, #25b7c4);
      font-family: 'Roboto', sans-serif;
    }

    section {
      margin: 50px;
    }


  </style>
</head>
<body>
    <h1>Listado citas pendientes </h1>
    <?php
    session_start();

    $conectar = mysqli_connect('localhost', "$_SESSION[rol]", "$_SESSION[rol]", 'consulta');
    if($_SESSION["rol"]=="medico"){
        $select = "SELECT * FROM citas where citMedico='$_SESSION[idusuario]' and citEstado='Asignado'";
        $consulta = mysqli_query($conectar, $select);
        echo "<div class='tbl-header'>";
        echo "<table cellpadding='0' cellspacing='0' border='0'>";
        echo "<thead>";        
        echo "<th>Fecha</th><th>Hora</th><th>Paciente</th><th>Medico</th><th>Consultorio</th><th>Estado</th><th>Atender</th>";
        echo "</thead>";
        echo "</table>";
        echo "</div>";
        echo "<div class='tbl-content'>";
        echo "<table cellpadding='0' cellspacing='0' border='0'>";
        echo "<tbody>";
          while ($valores = mysqli_fetch_array($consulta)) {
            $select1 = "SELECT medNombres FROM medicos where dniMed='$valores[citMedico]'";
            $select2 = "SELECT conNombre FROM consultorios where idConsultorio=$valores[citConsultorio] ";
            $consulta1= mysqli_query($conectar, $select1);
            $medico=mysqli_fetch_assoc($consulta1);
            $consulta2 = mysqli_query($conectar, $select2);
            $consultorio=mysqli_fetch_assoc($consulta2);
              echo "<tr>";
              echo "<td>$valores[citFecha]</td><td>$valores[citHora]</td><td>$valores[citPaciente]</td><td>$medico[medNombres]</td><td>$consultorio[conNombre]</td><td>$valores[citEstado]</td><td><form action='AtenderCita.php' method='POST'><button type='submit' value='$valores[citPaciente]' name='atender'>Atender</form></td>";
              echo "</tr>";
          }
          echo "</tbody>";
          echo "</table>";
          echo "</div>";
    }
    else{
      echo "<h3>No puede ver las citas pendientes, no es médico</h3>";
    }
    ?>
</body>
</html>