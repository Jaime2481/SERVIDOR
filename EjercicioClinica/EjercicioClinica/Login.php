<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        .login-page {
            width: 360px;
            padding: 8% 0 0;
            margin: auto;
        }

        .form {
            position: relative;
            z-index: 1;
            background: #FFFFFF;
            max-width: 360px;
            margin: 0 auto 100px;
            padding: 45px;
            text-align: center;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        }

        .form input {
            font-family: "Roboto", sans-serif;
            outline: 0;
            background: #f2f2f2;
            width: 100%;
            border: 0;
            margin: 0 0 15px;
            padding: 15px;
            box-sizing: border-box;
            font-size: 14px;
        }

        .form button {
            font-family: "Roboto", sans-serif;
            text-transform: uppercase;
            outline: 0;
            background: #4CAF50;
            width: 100%;
            border: 0;
            padding: 15px;
            color: #FFFFFF;
            font-size: 14px;
            -webkit-transition: all 0.3 ease;
            transition: all 0.3 ease;
            cursor: pointer;
        }

        .form button:hover,
        .form button:active,
        .form button:focus {
            background: #43A047;
        }

        .form .message {
            margin: 15px 0 0;
            color: #b3b3b3;
            font-size: 12px;
        }

        .form .message a {
            color: #4CAF50;
            text-decoration: none;
        }

        .form .register-form {
            display: none;
        }

        .container {
            position: relative;
            z-index: 1;
            max-width: 300px;
            margin: 0 auto;
        }

        .container:before,
        .container:after {
            content: "";
            display: block;
            clear: both;
        }

        .container .info {
            margin: 50px auto;
            text-align: center;
        }

        .container .info h1 {
            margin: 0 0 15px;
            padding: 0;
            font-size: 36px;
            font-weight: 300;
            color: #1a1a1a;
        }

        .container .info span {
            color: #4d4d4d;
            font-size: 12px;
        }

        .container .info span a {
            color: #000000;
            text-decoration: none;
        }

        .container .info span .fa {
            color: #EF3B3A;
        }

        body {
            background: #76b852;
            background: -webkit-linear-gradient(right, #76b852, #8DC26F);
            background: -moz-linear-gradient(right, #76b852, #8DC26F);
            background: -o-linear-gradient(right, #76b852, #8DC26F);
            background: linear-gradient(to left, #76b852, #8DC26F);
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            text-align: center;
        }
        span{
            color:red;
        }
    </style>
</head>

<body>
    <?php
    session_start();
    $ErrorUsuarioExiste= "";
    if (isset($_POST["cerrar"])) {
        session_destroy();
        echo "<br/><span>El Usuario ha cerrado sesion<span>";
    }
    if (isset($_POST["enviar"])) {
        $_SESSION["rol"] = "";
        $password = hash_hmac('sha512', $_POST["password"], 'secret');
        $conectar = mysqli_connect('localhost', 'root', '', 'consulta');
        $usuario = "SELECT dniUsu,usuLogin,usuPassword,usutipo from usuarios
        where dniUsu='$_POST[DNI]' and usuPassword='$password'";
        $select = mysqli_query($conectar, $usuario);
        $fila = mysqli_fetch_assoc($select);
        var_dump($fila);
        if (!empty($fila)) {
            $_SESSION["rol"] = $fila["usutipo"];
            $_SESSION["idusuario"] = $fila["dniUsu"];
            $_SESSION["nombre"] = $fila["usuLogin"];
        }
        if (empty($_SESSION["rol"])) {
            $ErrorUsuarioExiste= "El usuario no existe";
        } else {
            header("Location:botones.php"); 
        }
    }
    ?>
        <div class="login-page">
            <div class="form">
                <h1>Iniciar sesión</h1>
                <form action="#" method="POST" class="login-form">
                    <p>DNI </p>
                    <input type="text" name="DNI" required><br />
                    <br />
                    <p>Contraseña: </p>
                    <input type="password" name="password" required><span><?php echo $ErrorUsuarioExiste; ?></span>
<br />
                    <br />
                    <input type="submit" value="Iniciar sesión" name="enviar">
                </form>
            </div>
        </div>
</body>

</html>