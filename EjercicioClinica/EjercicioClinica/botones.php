<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        body {
            background: #76b852;
            /* fallback for old browsers */
            background: -webkit-linear-gradient(right, #76b852, #8DC26F);
            background: -moz-linear-gradient(right, #76b852, #8DC26F);
            background: -o-linear-gradient(right, #76b852, #8DC26F);
            background: linear-gradient(to left, #76b852, #8DC26F);
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            text-align: center;
        }
        </style>
</head>
<body>
    
<?php
    session_start();
if ($_SESSION["rol"]== "Administrador") {
    echo "<br/><h2>Bienvenido $_SESSION[nombre] usted se ha regristado como $_SESSION[rol]</h2>";

                $_SESSION["rol"] = "administrador";
        ?>
                <br/><h3>Acciones</h3> <br/>
                <form method="POST" action="AltaPaciente.php">
                    <input type="submit" value="Alta Paciente" class="btn btn-warning" >
                </form>
                <br />
                <form method="POST" action="AltaMedico.php">
                    <input type="submit" value="Alta Medico" class="btn btn-warning" >
                </form>
                <BR />
                <form method="POST" action="#">
                    <input type="submit" name="cerrar" class="btn btn-warning" value="Cerrar sesion">
                </form>
            <?php
            } else if ($_SESSION["rol"] == "Medico") {
                echo "<br/><h2>Bienvenido $_SESSION[nombre] usted se ha regristado como $_SESSION[rol]</h2>";
                $_SESSION["rol"] = "medico";

            ?>
                 <br/><h3>Acciones</h3> <br/>
                <form method="POST" action="VerCitasAtendidas.php">
                <input type="submit" class="btn btn-warning" value="Ver Citas Atendidas">
                </form>
                <br />
                <form method="POST" action="VerCitasPendientes.php">
                    <input type="submit" class="btn btn-warning" value="Ver Citas Pendientes">
                </form>
                <br />
                <form method="POST" action="VerPacientes.php">
                    <input type="submit" value="Ver Pacientes" class="btn btn-warning" >
                </form>
                <br>
                <form method="POST" action="#">
                    <input type="submit" name="cerrar" value="Cerrar sesion" class="btn btn-warning" >
                </form>
            <?php

            } else if ( $_SESSION["rol"]== "Asistente") {
                echo "<br/><h2>Bienvenido $_SESSION[nombre] usted se ha regristado como $_SESSION[rol]</h2>";
                $_SESSION["rol"] = "asistente";

            ?>
                <br/><h3>Acciones</h3><br/>
                <form method="POST" action="VerCitasAtendidas.php">
                    <input type="submit" value="Ver Citas Atendidas" class="btn btn-warning" >
                </form>
                <br />
                <form method="POST" action="NuevaCita.php">
                    <input type="submit" value="Nueva Cita" class="btn btn-warning" >
                </form>
                <br />
                <form method="POST" action="AltaPaciente.php">
                    <input type="submit" value="Alta Paciente" class="btn btn-warning" >
                </form><br />
                <form method="POST" action="VerPacientes.php">
                    <input type="submit" value="Ver Pacientes" class="btn btn-warning" >
                </form>
                <br>
                <form method="POST" action="#">
                    <input type="submit" name="cerrar" value="Cerrar sesion" class="btn btn-warning" >
                </form>
            <?php

            } else if ($_SESSION["rol"] == "Paciente") {
                echo "<br/><h2>Bienvenido $_SESSION[nombre] usted se ha regristado como $_SESSION[rol]</h2>";
                $_SESSION["rol"] = "paciente";
            ?>
                <br/><h3>Acciones</h3><br/>
                <form method="POST" action="VerCitas.php">
                    <input type="submit" value="Ver Citas" class="btn btn-warning" >
                </form>
                <br />
                <form method="POST" action="#">
                    <input type="submit" name="cerrar" value="Cerrar sesion" class="btn btn-warning" >
                </form>
    <?php

            }
?>
</body>
</html>