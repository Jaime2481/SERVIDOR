<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .login-page {
            width: 500px;
            padding: 8% 0 0;
            margin: auto;
        }

        .form {
            position: relative;
            z-index: 1;
            background: #FFFFFF;
            max-width: 360px;
            margin: 0 auto 100px;
            padding: 45px;
            text-align: center;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        }

        .form input {
            font-family: "Roboto", sans-serif;
            outline: 0;
            background: #f2f2f2;
            width: 100%;
            border: 0;
            margin: 0 0 15px;
            padding: 15px;
            box-sizing: border-box;
            font-size: 14px;
        }

        .form select {
            font-family: "Roboto", sans-serif;
            outline: 0;
            background: #f2f2f2;
            width: 100%;
            border: 0;
            margin: 0 0 15px;
            padding: 15px;
            box-sizing: border-box;
            font-size: 14px;

        }

        .form button {
            font-family: "Roboto", sans-serif;
            text-transform: uppercase;
            outline: 0;
            background: #4CAF50;
            width: 100%;
            border: 0;
            padding: 15px;
            color: #FFFFFF;
            font-size: 14px;
            -webkit-transition: all 0.3 ease;
            transition: all 0.3 ease;
            cursor: pointer;
        }

        .form button:hover,
        .form button:active,
        .form button:focus {
            background: #43A047;
        }

        .form .message {
            margin: 15px 0 0;
            color: #b3b3b3;
            font-size: 12px;
        }

        .form .message a {
            color: #4CAF50;
            text-decoration: none;
        }

        .form .register-form {
            display: none;
        }

        .container {
            position: relative;
            z-index: 1;
            max-width: 300px;
            margin: 0 auto;
        }

        .container:before,
        .container:after {
            content: "";
            display: block;
            clear: both;
        }

        .container .info {
            margin: 50px auto;
            text-align: center;
        }

        .container .info h1 {
            margin: 0 0 15px;
            padding: 0;
            font-size: 36px;
            font-weight: 300;
            color: #1a1a1a;
        }

        .container .info span {
            color: #4d4d4d;
            font-size: 12px;
        }

        .container .info span a {
            color: #000000;
            text-decoration: none;
        }

        .container .info span .fa {
            color: #EF3B3A;
        }

        body {
            background: #76b852;
            /* fallback for old browsers */
            background: -webkit-linear-gradient(right, #76b852, #8DC26F);
            background: -moz-linear-gradient(right, #76b852, #8DC26F);
            background: -o-linear-gradient(right, #76b852, #8DC26F);
            background: linear-gradient(to left, #76b852, #8DC26F);
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            text-align: center;
        }

        span {
            color: red;
        }
    </style>
</head>

<body>
    <?php $hoy = date("Y-m-d"); $time =date("h:i:s");
    session_start();
    if ($_SESSION["rol"] == "asistente") {
        $conectar = mysqli_connect('localhost', "$_SESSION[rol]", "$_SESSION[rol]", 'consulta');
        $ErrorFecha = "";
        $ErrorHora = "";
        if (isset($_POST["enviar"])) {
            if ($_POST["date"] < $hoy) {
                $ErrorFecha = " Fecha invalida<br/>";
            }
            else if($_POST["date"] == $hoy && $_POST["time"]<=$time){
                $ErrorHora = " Hora inválida<br/>";

            }
            $insert = "INSERT INTO citas (idCita,citFecha,citHora,citPaciente,citMedico,citConsultorio,citEstado) VALUES (NULL,'$_POST[date]', '$_POST[time]', '$_POST[paciente]', '$_POST[medico]', $_POST[consultorio], 'Asignado')";
            $select = mysqli_query($conectar, $insert);
            echo "<h3>Cita creada correctamente</h3>";
        }

    ?>
        <div class="login-page">
            <div class="form">
                <h1>Asignar Cita</h1>
                <form method="POST" action="#" class="login-form">
                    <label>Paciente:</label>
                    <select name="paciente">
                        <option selected>Seleccione</option>
                        <?php
                        $pacientes = "SELECT * FROM pacientes";
                        $select = mysqli_query($conectar, $pacientes);
                        while ($valores = mysqli_fetch_array($select)) {
                            echo "<option value=" . $valores["dniPac"] . ">$valores[pacNombres]</option>";
                        }
                        ?>
                    </select>
                    <br /><br />
                    <label>Fecha</label>
                    <input type="date" name="date" min="<?php echo $hoy; ?>"><span><?php if (isset($_POST["enviar"])) {
                                                                                        echo $ErrorFecha;
                                                                                    } ?></span>
                    <br><br />
                    <label>Hora</label>
                    <input type="time" name="time" ><span><?php if (isset($_POST["enviar"])) {
                                                                                        echo $ErrorHora;
                                                                                    } ?></span>
                    <br><br />
                    <label>Medico</label>
                    <select name="medico">
                        <option selected>Seleccione</option>
                        <?php
                        $medicos = "SELECT * FROM medicos";
                        $select = mysqli_query($conectar, $medicos);
                        while ($valores = mysqli_fetch_array($select)) {
                            echo "<option value=" . $valores["dniMed"] . ">$valores[medNombres]</option>";
                        }
                        ?>
                    </select>
                    <br /><br />
                    <label>Consultorio</label>
                    <select name="consultorio">
                        <option selected>Seleccione</option>
                        <?php
                        $consultorios = "SELECT * FROM consultorios";
                        $select = mysqli_query($conectar, $consultorios);
                        while ($valores = mysqli_fetch_array($select)) {
                            echo "<option value=" . $valores["idConsultorio"] . ">$valores[conNombre]</option>";
                        }
                        ?>
                    </select>
                    <br><br />
                    <input type="submit" name="enviar" value="Enviar">
                </form>
            </div>
        </div>
    <?php
    } else {
        echo "El rol no es ni Asistente, no tiene permiso";
    } ?>
</body>

</html>