<?php
session_start();
function recoge($var)
{
    $tmp=(isset($_REQUEST[$var]))
    ? trim(htmlspecialchars($_REQUEST[$var], ENT_QUOTES, "UTF-8"))
    : "";
return $tmp;
}

$accion = recoge("action");
$accionOk = false;

if($accion != "cero" && $accion != "subir" && $accion != "bajar"){
    header("Location:Inicio.php");
    exit;
}
else{
    $accionOk = true;
}
if($accionOk){
    if($accion == "cero"){
        $_SESSION["numero"]=0;
    }
    elseif($accion == "subir"){
        $_SESSION["numero"]++;
    }
    elseif($accion == "bajar"){
        if($_SESSION["numero"]>0){
        $_SESSION["numero"]--;}
    }
    header("Location:Inicio.php");
    exit;
}
?>