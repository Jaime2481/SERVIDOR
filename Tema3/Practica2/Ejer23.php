<html>
<head>
    <style>
        table{
            border:2px solid black;
            border-collapse: collapse;
        }
        th{
            border:2px solid black;
            border-collapse: collapse;
            padding: 4px;
            text-align: center;
        }
        td{
            border:2px solid black;
            border-collapse: collapse;
            padding: 8px;
            text-align: center;
            color:red;
        }
        tr{
            border:2px solid black;
            border-collapse: collapse;

        }
    </style>
</head>
<body>
<table>
<?php
$estadios_futbol=array(
    'Barcelona' => 'Camp Nou',
    'Real Madrid' => 'Santiago Bernabeu',
    'Valencia' => 'Mestalla',
    'Real Sociedad' =>   'Anoeta'
);
unset($estadios_futbol['Real Madrid']);
foreach ($estadios_futbol as $key => $value) {
    echo "<tr>";
    echo "<th> ".$key."</th><td> ".$value."</td>";
    echo "<tr>";
}

?>
</body>
</html>