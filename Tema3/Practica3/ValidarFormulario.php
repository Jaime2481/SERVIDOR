
<html lang="es">
<head>
    <style>
        span{
            color: red;
        }
    </style>
</head>
<body>
    <?php
    $nombre=$_POST['name'];
    $nombreErr="";
    $generoErr="";
    if (!preg_match("/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ\s]+$/",$nombre)) {
        $nombreErr="El nombre tiene caracteres inválidos";
    }
    if(empty($nombre)){
        $nombreErr="Nombre requerido";
    }
    $email=$_POST['email'];
    $emailErr="";
     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
     $emailErr = "Fomato de Email invalido";
     }
     if(empty($email)){
         $emailErr="Email requerido";
     }
     $url = $_POST['url'];
     $url = filter_var($url, FILTER_SANITIZE_URL);
     $urlErr="";
     if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
     } else {
         $urlErr="Invalid URL ";
     }
     $comentario=$_POST['comment'];
    ?>
    <h1>PHP Form Validation Example</h1>
    <span>* required field</span>
    <br><br>
<form method="POST" action="ValidarFormulario.php">
    Nombre: <input type="text" name="name" value="<?php echo $nombre ?>"  >
     <span>* <?php echo $nombreErr ?></span><br><br>
    E-mail: <input type="text" name="email" value="<?php echo $email ?>" >
    <span>* <?php echo $emailErr ?></span><br><br>
    <?php $UrlErr="Invalid Url" ?>
    Website: <input type="text" name="url" value="<?php echo $url ?>">
    <span> <?php echo $urlErr ?></span><br><br>
    Comentarios:
    <textarea name="comment" rows=5 cols=40 ><?php echo $comentario; ?></textarea>
    <br><br>
    Gender:
    <input type="radio" name="sexo" <?php if (!isset($_POST['sexo'])){ $generoErr="Gender required";} ?> 
    value="mujer"> Mujer
<input type="radio" name="sexo" <?php if(!isset($_POST['sexo'])){ $generoErr="Gender required";} ?> value="hombre"> Hombre
 <span>* <?php echo $generoErr ?></span>
<br><br>
  <input value="Enviar" type="submit" name="boton" />
  <br><br><br><br>
</form>
<h1>TUS DATOS: </h1>
<?php
if(!empty($nombre)){
    echo "Nombre: ".$nombre."<br>";
}
if(!empty($email)){
    echo "E-Mail: ".$email."<br>";
}
if(!empty($url)){   
    echo "URL: ".$url."<br>";
}
if(!empty($comentario)){
    echo "Comentario: ".$comentario."<br>";
}
if(isset($_POST['sexo'])){ 
    echo "Sexo: ".$_POST['sexo'];
}
?>
</body>
</html>

