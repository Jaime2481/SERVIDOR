<!DOCTYPE html>
<html lang="es">
  <head>
    <meta  charset="utf-8" >
    <style>
        body{
            background-color: green;
        }
        .cuerpo{
            border: 8px solid darkgreen;
            width:650px;
            height:850px;
            background-color:white;
            float:left;
            margin-left:100px;
        }
        .cajas{
            float:right;
            width: 300px;
            height: 200px;
            margin-left:20px;
            margin-bottom:50px;
        }
        p{
            margin-left:20px;
            margin-bottom:50px;
        }
        h1{
            text-align:center;
            margin-bottom: 25px;
            font-family: "Comic Sans MS";
        }
        aside{
            float: right;
            margin-left:auto;
            background-color:white;
            border: 8px solid darkgreen;
            padding:10px;
            margin-right:100px;
        }
    </style>
  </head>
  <body>
    <div  style="display:inline;">
     <?php
     session_start();
     if(isset($_POST['usuario'])){
     $_SESSION=array(
        'usuario' => $_POST['usuario'],
        'carro' => array(
            'Television 4K' => 0,
            'Movil' => 0,
            'iPad' => 0,
            'Raton' => 0,
            'Alfombrilla' => 0,
            'Silla' => 0
        ),
     );
     };
    $_SESSION['Productos']=array(
        array(
            'Producto' => 'Television 4K',
            'Descripcion' => '22 pulgadas',
            'Precio' => 210
        ),
        array(
            'Producto' => 'Movil',
            'Descripcion' => 'Apple iPhone 12 Mini 64GB Blanco Libre',
            'Precio' => 739,98  
        ),
        array(
            'Producto' => 'iPad',
            'Descripcion' => 'Apple iPad Pro 2021 12,9" 128Gb Cellular Gris Espacial',
            'Precio' => 1348,75
        ),
        array(
            'Producto' => 'Raton',
            'Descripcion' => 'Logitech G403 Hero Ratón Gaming 16000DPI',
            'Precio' => 36,99
        ),
        array(
            'Producto' => 'Alfombrilla',
            'Descripcion' => ' Teclado Gaming Mecánico RGB Modular Cherry MX Red',
            'Precio' => 229,98
        ),
        array(
            'Producto' => 'Silla',
            'Descripcion' => 'Newskill Kitsune Silla Gaming Azul',
            'Precio' => 177,53
        ),
    ); 
        if(isset($_POST['restar'])){
            foreach($_SESSION['Productos'] as $array) {
                foreach ($array as $indice => $valor) {
                    if(strcmp($valor,$_POST['restar']) == 0){
                        if($_SESSION['carro'][$valor]>0){   
                        $_SESSION['carro'][$valor]=$_SESSION['carro'][$valor]-1;
                        }
                    }
                }
            }
        }
        if(isset($_POST['sumar'])){
        foreach($_SESSION['Productos'] as $array) {
            foreach ($array as $indice => $valor) {
                if(strcmp($valor,$_POST['sumar']) == 0) {
                    $_SESSION['carro'][$valor]=$_SESSION['carro'][$valor]+1;
                }
            }
        }
    }
   
            ?>
            <aside>
                <h1>Carrito de las estafas</h1>
                <?php
                $_SESSION['total']=0;
                $i=0;
                foreach($_SESSION['Productos'] as $array) {
                    $i++;
                    if($_SESSION['carro'][$array['Producto']] > 0){
                        ?>
                        <img src="<?php echo $i.".jpg" ?>" width=60 height=60 />
                        <br>                        <?php
                        echo $array['Producto'].": ".$_SESSION['carro'][$array['Producto']]." = ".($_SESSION['carro'][$array['Producto']]*$array['Precio'])."€";
                        ?>
                        <form method="POST">
                        <button value="<?php echo $array['Producto'] ?>" type="submit" name="restar">Restar</button>
                    </form>
                        <?php
                        echo "<br>";
                        $_SESSION['total']=  $_SESSION['total']+($_SESSION['carro'][$array['Producto']]*$array['Precio']);
                     }
                    }
                    if($_SESSION['total'] !== 0){
                    echo "<br>Total: ".  $_SESSION['total']."€"; 
                    }
                  ?>
                <form method="POST" action="pagar.php">
                    <br>
                    <input style="margin: auto;" type="submit" name="pagar" value="Pulse para pasar a pagar" />
                </form>
            </aside>

     <div class="cuerpo">
    <h1>Bienvenido/a <?php echo  $_SESSION['usuario'] ?></h1>
    <?php
    $i=0;
    foreach($_SESSION['Productos'] as $array) {
        echo '<div class="cajas">';
        $i++;
        ?>
        <img src="<?php echo $i.".jpg" ?>" width=100 height=100 />
        <br/>
        <?php
        echo "Producto: ".$array["Producto"]."<br/>";
        echo "Descripcion: ".$array["Descripcion"]."<br/>";
        echo "Precio: ".$array["Precio"]."€<br/>";
        ?>
        <form method="POST">
            <br>
        <button value="<?php echo $array['Producto'] ?>" type="submit" name="sumar">Añadir</button>
    </form>
        <br/>
        </div>
        <?php
}
    ?>  
    <br><br>
  </div>
</body>
</html>