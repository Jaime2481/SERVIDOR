<html lang="es">
<head>
    <style>
      table{
        color: purple;
        font-family: "Comic Sans MS";
        background-color: #D3D3D3;
        border: 2px solid purple;
        margin: auto;
        border-collapse: collapse;
      }
      tr{
        text-align: center;
        border: 2px solid purple;
      }
      h1{
        text-align: center;
        font-family: "Comic Sans MS";
      }
      h3{
        text-align: center;
        font-family: "Comic Sans MS";
      }
      td{
        padding: 4px;
      }
      th{
        padding: 4px;
        border: 2px solid purple;
      }
      </style>
</head>
<body>
<?php
 if (isset($_POST['boton'])){
   echo "<h1>CALENDARIO</h1>";
 
  $fecha= $_POST['ano']."/".$_POST['mes']."/15";
  $i = strtotime($fecha);
  $dia =jddayofweek(cal_to_jd(CAL_GREGORIAN, date("m",$i),date("d",$i), date("Y",$i)) , 0 );
  $dias = array('','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
  $aux = array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
  $mes = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
  $number = cal_days_in_month(CAL_GREGORIAN, $_POST['mes'], $_POST['ano']);
  echo '<h3> '.$mes[$_POST['mes']-1].' del '. $_POST['ano'].'</h3>';
  echo " <table>";
  echo "<tr>";
  for($i=0;$i<=6;$i++) {
    echo '<th>'.$aux[$i].'</th>  ';
  }
  echo "</tr>";
  $cont=0;
  $primera_fila= true;
  if($dia==0){
    $dia=7;
  }
  while($cont<$number ){ 
   echo "<tr>";
    if($primera_fila){
      for($z=1;$z<=7;$z++){
        $primera_fila = false;
        if($z>=$dia){
          $cont++;
  
              echo '<td style="border: 2px solid purple;">'.$cont.'</td>  ';
        }
        else{
          echo "<td></td>";
        }
    }
  }
    else{
      for($y=1;$y<=7;$y++){
        $cont++;
        echo '<td style="border: 2px solid purple;">'.$cont.'</td>  ';
        if($cont==$number){
          break;
        }
    }
  }
  echo "</tr>";
}
echo "</table>";
}
else{
?>
<form method="POST">
    Introducir mes :
      <input maxlength="2" type="numero" name="mes">
       <br/>   <br/>
    Introducir año :
      <input maxlength="4" type="numero" name="ano">
       <br/>   <br/>
  <input value="Aceptar" type="submit" name="boton"  />
</form>
  <?php
}
?>
</body>
</html>
