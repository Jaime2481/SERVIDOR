<?php
/**
 * Juego de dados- dados-1.php
 *
 * @author Escriba aquí su nombre
 *
 */
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>
    Dados iguales o distintos. Juego de dados.
    Sin formularios.
    Escriba aquí su nombre
  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="mclibre-php-ejercicios.css" title="Color">
</head>

<body>
  <h1>Juego de dados - Dados iguales o distintos</h1>

  <p>Actualice la página para mostrar otra partida.</p>

  <table>
    <tr>
      <th colspan="2">Jugador 1<br>(coincide un dado)</th>
      <th colspan="2">Jugador 2<br>(no coinciden)</th>
    </tr>
<?php

print "  <p class=\"aviso\">Ejercicio incompleto</p>\n";

?>

  <footer>
    <p>Escriba aquí su nombre</p>
  </footer>
</body>
</html>
