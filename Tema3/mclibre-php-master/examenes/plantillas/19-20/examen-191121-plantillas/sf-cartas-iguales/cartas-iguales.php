<?php
/**
 * Cartas iguales - cartas-iguales.php
 *
 * @author Escriba aquí su nombre
 */
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>
    Cartas iguales.
    Sin formularios.
    Escriba aquí su nombre
  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="mclibre-php-ejercicios.css" title="Color">
</head>

<body>
  <h1>Cartas iguales</h1>

  <p>Cada jugador saca 10 cartas. Se comparan una a una. Si no coincide ninguna, gana el jugador A. Si sólo coincide una, gana el jugador B. Si coinciden más de una, empatan.</p>

<?php

print "<p class=\"aviso\">Ejercicio incompleto</p>\n";

?>
  <footer>
    <p>Escriba aquí su nombre</p>
  </footer>
</body>
</html>
