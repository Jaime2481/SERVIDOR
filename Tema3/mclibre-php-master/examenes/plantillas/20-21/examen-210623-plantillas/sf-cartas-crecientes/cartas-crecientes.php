<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>
    Cartas crecientes.
    Escriba aquí su nombre
  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="mclibre-php-ejercicios.css" title="Color">
</head>

<body>
  <h1>Cartas crecientes</h1>

  <p>Cada jugador saca 5 cartas. Se compara cada una con la anterior. Si es mayor, se suma su valor. Si es menor, se resta. Si es igual, se descarta. El valor de la primera carta se suma siempre.</p>

<?php

print "  <p class=\"aviso\">Ejercicio incompleto</p>\n";

?>

  <footer>
    <p>Escriba aquí su nombre</p>
  </footer>
</body>
</html>
