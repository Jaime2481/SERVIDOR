<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        body {
            text-align: center;
            background: url(fondo.jpg) center / cover no-repeat;
            height: 97.7vh;
            margin: 0;
            padding: 0;
        }
        table {
            border: 2px solid white;
            border-collapse: collapse;
            text-align: center;
            margin: 20px auto;
            color: white;
            font-family: sans-serif;
            backdrop-filter: blur(5px);
        }
        td{
            border: 2px solid white;
            padding:8px;
        }
        th{
            border: 2px solid white;
            padding:5px;
        }
        form{
            margin:20px;
        }
        h1{
            font-family: "Comic Sans MS";
            text-align:center;
            color:blue;
        }
    </style>
</head>
<body>
    <h1>Articulos Disponibles</h1>  
    <?php
        session_start();
        $conectar=mysqli_connect('localhost', $_SESSION["rol"],$_SESSION["rol"],'ventas');
        $usuario="SELECT  * from articulos";
        $select=mysqli_query($conectar,$usuario);
        echo "<table>";
        echo "<th>ID</th><th>Descripcion</th><th>Precio</th><th>Caracteristicas</th>";
          while ($valores = mysqli_fetch_array($select)) {
              echo "<tr>";
            echo "<td>$valores[Id_articulo]</td><td>$valores[Descripcion]</td><td>$valores[Precio]€</td><td>$valores[Caracteristicas]</td> ";
            echo "</tr>";
          }
          echo "</table>";
    ?>
    <form action="Practica1A.php" method="POST">
        <input type="submit" value="Volver al inicio">
    </form>
    <?php

    ?>
</body>
</html>