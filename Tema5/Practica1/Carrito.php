<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <style>
        body {
            text-align: center;
            background: url(fondo.jpg) center / cover no-repeat;
            height: 97.7vh;
            margin: 0;
            padding: 0;
        }

        table {
            border: 2px solid white;
            border-collapse: collapse;
            text-align: center;
            margin: 20px auto;
            color: white;
            font-family: sans-serif;
            backdrop-filter: blur(5px);
        }

        td {
            border: 2px solid white;
            color: white;
            padding: 15px;
            font-size: 20px;
        }

        th {
            border: 2px solid white;
            color: white;
            padding: 5px;
        }

        .boton {
            display: inline;
        }

        h1 {
            font-family: "Comic Sans MS";
            text-align: center;
            color: blue;
            font-size: 40px;
        }

        div {
            float: left;
            margin-top: 20px;
            margin-right: 20px;
            margin-left: 80px;
            font-size: 17px;
        }

        .red {
            color: red;
        }
    </style>
</head>

<body>
    <div>
        <h1>Carrito de productos</h1>
        <?php
        session_start();
        if (isset($_POST["Carrito"])) {
            $_SESSION['compra'] = array();
        }
        $conectar = mysqli_connect('localhost', $_SESSION["rol"], $_SESSION["rol"], 'ventas');
        $table = "SELECT  * from articulos";
        $select = mysqli_query($conectar, $table);
        echo "<table>";
        $id = 0;
        echo "<th>ID</th><th>Descripcion</th><th>Precio</th><th>Caracteristicas</th><th>Añadir al carrito</th>";
        while ($valores = mysqli_fetch_array($select)) {
            echo "<tr>";
            echo "<td>$valores[Id_articulo]</td><td>$valores[Descripcion]</td><td class='red'>$valores[Precio]€</td><td>$valores[Caracteristicas]</td><td>  
                  <form action='#' method='POST'><button type='submit' value=$valores[Id_articulo] name='sumar'>+</button></form>
                  </td>";
            echo "</tr>";
        }
        echo "</table>";
        ?>
        <form class="boton" action="CerrarSesion.php">
            <input type="submit" value="Cerrar">
        </form>
        <form action="Practica1A.php" method="POST" class="boton">
            <input type="submit" value="Volver al inicio">
        </form>
    </div>
    <?php
    if (isset($_POST["sumar"])) {
        $producto = false;
        $compra[0] = $_SESSION["idusuario"];
        $compra[1] = $_POST["sumar"];
        $compra[2] = date('Y-m-d h:i:s');
        $compra[3] = 1;
        for ($x = 0; $x < count($_SESSION['compra']); $x++) {
            if ($_SESSION["compra"][$x][1] == $compra[1]) {
                $_SESSION["compra"][$x][3]++;
                $producto = true;
            }
        }
        if (!$producto) {
            array_push($_SESSION["compra"], $compra);
        }
    }
    if (isset($_POST["restar"])) {
        for ($x = 0; $x < count($_SESSION['compra']); $x++) {
            if ($_POST["restar"] == $_SESSION["compra"][$x][1]) {
                $_SESSION["compra"][$x][3]--;
                if ($_SESSION["compra"][$x][3] == 0) {
                    array_splice($_SESSION["compra"], $x, 1);
                }
            }
        }
    }



    ?>
    <div>
        <h1>Articulos en el carrito</h1>
        <?php

        $total = 0;
        for ($z = 0; $z < count($_SESSION['compra']); $z++) {
            $id = $_SESSION['compra'][$z][1];
            $aux = "SELECT precio from articulos where Id_articulo='$id'";
            $select = mysqli_query($conectar, $aux);
            $precioArticulo = mysqli_fetch_assoc($select);
            echo "Id: " . $_SESSION["compra"][$z][1] . " Cantidad: " . $_SESSION["compra"][$z][3] . " Precio unidad: " . $precioArticulo['precio'] . " Precio Unitario: " . ($_SESSION['compra'][$z][3] * $precioArticulo["precio"]) . " ";
            echo "<form action='#' method='POST' class='boton'><button type='submit' value='$id' name='restar'>-</button></form><br/>";
            $total += $_SESSION["compra"][$z][3] * $precioArticulo["precio"];
        }
        echo "<h3>Precio total de la compra: $total</h3>";
        ?>
        <form action="Pagar.php" method="POST">
            <input type="submit" value="Proceder al pago">
        </form>
    </div>
</body>

</html>