<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <style>
        body {
            text-align: center;
            background: url(fondo.jpg) center / cover no-repeat;
            height: 97.7vh;
            margin: 0;
            padding: 0;
        }

        h1 {
            font-family: "Comic Sans MS";
            color: blue;
        }
    </style>
</head>

<body>
    <h1>Hasta la proxima :)</h1>
    <?php
    session_start();
    session_destroy();
    ?>
    <form action="Practica1A.php" method="POST">
        <input type="submit" value="Volver al indice">
    </form>
    <?php
    ?>
</body>

</html>