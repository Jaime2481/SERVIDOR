<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <style>
        form {
            font-family: "Comic Sans MS";
            text-align: center;
        }

        h1 {
            font-family: "Comic Sans MS";
            text-align: center;
            color: blue;
        }
        p {
            display: inline;
            font-size: 20px;
        }
        body {
            text-align: center;
            background: url(fondo.jpg) center / cover no-repeat;
            height: 97.7vh;
            margin: 0;
            padding: 0;
        }
    </style>
</head>

<body>
    <?php
    session_start();
    if (!isset($_POST["enviar"])) {
    ?>

        <h1>Iniciar sesión</h1>
        <form action="#" method="POST">
            <p>Login del usuario: </p>
            <input type="text" name="login" required><br />
            <br />
            <p>Contraseña: </p>
            <input type="password" name="password" required><br />
            <br />
            <input type="submit" value="Iniciar sesión" name="enviar">
        </form>
    <?php
    }
    if (!isset($_POST["crear"]) && !isset($_POST["enviar"])) {
    ?>
        <h1>Si aun no tienes cuenta</h1>
        <form class="form" action="Usuario.php">
            <p>Puede registrase aquí: </p>
            <input type="submit" value="Registrarse">
        </form>
        <?php
    }
    if (isset($_POST["enviar"])) {
        $_SESSION["rol"] = "";
        $password = hash_hmac('sha512', $_POST["password"], 'secret');
        $conectar = mysqli_connect('localhost', 'root', '', 'ventas');
        $usuario = "SELECT  idusuario,usuario,password,rol from usuarios
        where usuario='$_POST[login]' and password='$password'";
        $select = mysqli_query($conectar, $usuario);
        $fila = mysqli_fetch_assoc($select);
        if (!empty($fila)) {
            $_SESSION["rol"] = $fila["rol"];
            $_SESSION["idusuario"] = $fila["idusuario"];
        }
        if (empty($_SESSION["rol"])) {
            echo "<br/>Usuario: $_POST[login]<br/>Contraseña: $password<br/>El usuario intrducido no existe";
        } else {

            if ($_SESSION["rol"] == "administrador") {
        ?>
                <h1>Acciones</h1>
                <form method="POST" action="Articulo.php">
                    <p>Añadir productos: <p>
                            <input type="submit" value="Nuevo Producto" name="Insertar">
                </form>
                <br/>
                <form method="POST" action="Consultar.php">
                    <p>Consultar la tabla productos: <p>
                            <input type="submit" value="Consultar" name="Consultar">
                </form>
            <?php
            } else if ($_SESSION["rol"] == "consultor") {
            ?>
                <h1>Acciones</h1>
                <form method="POST" action="Consultar.php">
                    <p>Consultar la tabla productos: <p>
                            <input type="submit" value="Consultar" name="Consultar">
                </form>
                <br/>
                <form method="POST" action="Carrito.php">
                    <p>Consultar el carrito: <p>
                            <input type="submit" value="Carrito" name="Carrito">
                </form>
                <br/>
                <form method="POST" action="Compras.php">
                    <p>Consultar tus compras: <p>
                            <input type="submit" value="Compras" name="Compras">
                </form>
    <?php

            }
        }
    }
    ?>
</body>

</html>