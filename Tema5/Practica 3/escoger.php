<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style.css">
</head>


<body>
	<?php
	session_start();
	?>
	<div class="container">
		<form class="formulario" action="#" method="post">
			<p><?php echo 'Conectado el usuario ' . $_SESSION["usuario"] . ' con el rol ' . $_SESSION["rol"] ?></p>
			<?php
			if ($_SESSION["rol"] === "consultor") {
				echo '
				<div class="form-group">
					<button type="submit" name="comprar">Comprar Articulos</button>
				</div>
				<div class="form-group">
					<button type="submit" name="consultarcompras">Consultar Compras</button>
				</div>
				';
			} else if ($_SESSION["rol"] === "administrador") {
				echo '
				<div class="form-group">
					<button type="submit" name="introducir">Introducir Articulos</button>
				</div>
				<div class="form-group">
					<button type="submit" name="consultar">Consultar Articulos</button>
				</div>
				';
			}
			?>
		</form>
	</div>
	<?php
	if (isset($_POST["comprar"])) {
		header("Location: carritocompra.php");
	} else if (isset($_POST["consultarcompras"])) {
		header("Location: consultarcompras.php");
	} else if (isset($_POST["introducir"])) {
		header("Location: introducir.php");
	} else if (isset($_POST["consultar"])) {
		header("Location: consultar.php");
	}
	?>
</body>

</html>