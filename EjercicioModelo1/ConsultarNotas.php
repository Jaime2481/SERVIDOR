<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<?php
	session_start();
	$sessionErr = "";
		$_SESSION["connection"] = mysqli_connect("localhost", "$_SESSION[rol]", "$_SESSION[rol]", "cifp");
	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:index.php");
	}
	$suspensas = "SELECT count(nota) as Suspensas from notas where alumno='$_SESSION[id]' and nota<'5'";
	$consultaf = mysqli_query($_SESSION["connection"], 'SELECT notas.asignatura,notas.fecha,notas.nota FROM notas,usuarios WHERE notas.alumno=usuarios.id AND usuarios.login="'.$_SESSION['idusuario'].'"');
	$select = mysqli_query($_SESSION["connection"], $suspensas);
	$NumeroSuspensas = mysqli_fetch_assoc($select);
	echo '
		<div class="container">
			<div class="consulta">
				<form action="#" method="post">
					<div class="flex space-between">
						<p>Conectado el usuario ' . $_SESSION["idusuario"] . ' con el rol ' . $_SESSION["rol"] . '</p>
						<button type="submit" class="cerrar" name="cerrarsesion">Cerrar sesion</button>
					</div>
					<p class="error">' . $sessionErr . '</p>
					<table>
						<thead>
							<th>Asignatura</th>
							<th>Fecha</th>
							<th>Nota</th>
						
						</thead>
						<tbody>
	';

	while ($fila = mysqli_fetch_array($consultaf)) {
		echo '
							<tr>
								<td> ' . $fila["asignatura"] . '</td>
								<td> ' . $fila["fecha"] . '</td>
								<td> ' . $fila["nota"] . ' </td>
							</tr>
		';
	}
	echo "
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<p>Ha suspendido $NumeroSuspensas[Suspensas] materia</p>";

	?>
</body>